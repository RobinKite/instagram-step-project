# Instagram (step-project FE_17)

This project is hosted on [Netlify](https://instagram-step-project-fe17.netlify.app/).<br/>

Welcome to our Instagram-like step-project, a collaborative initiative led by Alex Kite. Our goal in this endeavor was to emulate the functionalities of the real Instagram app, bringing together the creative efforts of our talented team. Despite a two-week timeline, we've managed to make substantial progress, resulting in the implementation of several key features.

## 🔑 Key Features Implemented:

- User Page and Home Page: We've successfully created functional User and Home pages, mimicking the core experience of Instagram.

- Authentication: Our app boasts a fully operational login and registration system, ensuring a secure and personalized user experience.

- Posts and Interactions: Users can view posts from other individuals, create their own posts, and even engage with content by leaving comments and likes.

- Social Connectivity: We've implemented the ability to follow other users, aggregating their posts on your home feed. Alternatively, you can explore their dedicated profile pages.

- Theme Customization: Enjoy a dynamic visual experience with the option to switch between light and dark color themes.

## 📝 API Documentation

In addition we're excited to provide you with an overview of the existing API server methods. These methods serve as the backbone of our app, facilitating seamless communication between the front-end and back-end functionalities.
You can find more detailed version right here: [Google Docs](https://docs.google.com/document/d/1uSZevQDhZJ7pCM17GG8HKdR4sSgJMoRfEUnnfZRUsu8/)

<details>
<summary>Click to expand</summary>

- User Authentication:
  - POST `/api/v1/auth/register`: Register a new user with the provided credentials, specifying username, password, firstName, and lastName in the body of the request.
  - POST `/api/v1/auth/login`: Log in an existing user using their credentials, specifying username and password in the request body.

- User Operations:
  - GET `/api/v1/user?username={username}`: Get a specific user profile information.
  - GET `/api/v1/user/feed`: Get posts from the users you are subsribed to.
  - POST `/api/v1/user/subscription`: Follow a specific user, specifying the target user's username in the body of the request
  - DELETE `/api/v1/user/subscription?username={username}`: Unfollow a specific user.

- Posts Operations:
  - GET `/api/v1/post?postId={postId}`: Get a specific post information.
  - POST `/api/v1/post`: Add a new post, specifying imageUrl in the body of the request. You can also include the optional caption and location fields.
  - DELETE `/api/v1/post?id={postId}`: Remove a specific post.

  - POST `/api/v1/post/comment`: Add a comment to a specific post, specifying postId and text in the request body.
  - DELETE`/api/v1/post/comment?commentId={commentId}`: Remove a specific comment.

  - POST `/api/v1/post/like`: Like a specific post, specifying postId in the body of the request.
  - DELETE `/api/v1/post/like?postId={id}`: Remove like from a specific post.
</details>

## 💻 Technologies

The following technologies were used in this project:

- Back-end: 
  - Python
  - FastAPI
  - SQLalchemy
  - DigitalOcean (deployment)

- Front-end:
  - React
  - React Router 
  - Redux Toolkit
  - React icons and loader
  - Infinite scroll
  - Formik and Yup
  - ESLint
  - Tailwind CSS
  - Vite
  - Netlify (deployment)

## 🧑‍💼 Project Management

- Trello

## 🐥 Contributors

- Alex Kite 🦅 - project manager 👨‍💼
- Bohdan 😽 - backend developer 👨‍💻

- Team Companion Koalas:
  - Oksana 🐨 - team leader ⭐️
  - Tanya 🪐
  - Anya 🐹

- Team Fabulous Ostriches:
  - Roma 🦀 - team leader ⭐️
  - Snizhana
  - Yaroslav 🦥
  - Dima 🤿

- Team Superhero Elephants:
  - Yura 👻 - team leader ⭐️
  - Maksim 🙈
  - Nastya 🍀

  This project wouldn't have been possible without the dedication and collaborative spirit of each team member. We're excited to present our progress so far and look forward to further enhancing the app's functionalities.

## 📃 Task Distribution

- Alex Kite: 
  - Team management;
  - Project organizations;
  - Code refactor & bug fixing;
  - Refinements & enhancements across the entire codebase;
  - Authentication implementation;

- Bohdan:
  - Project workspace setup;
  - Full back-end development;
  - Client service;

- Oksana (Team Companion Koalas - team leader): 
  - Team Management and Coordination;
  - Header integration and functionality;
  - Comment container development;
  - Post interaction features;
  - Post creation date display;
  - Infinite scroll implementation;

- Anya (Team Companion Koalas): 
  - Comment item component addition;
  - Delete post and comment functionality;
  - Code fixes and enhancements;

- Tanya (Team Companion Koalas): 
  - Footer addition;
  - Route organization;

- Roma (Team Fabulous Ostriches - team leader):
  - Team Management and Coordination;
  - Redux toolkit integration;
  - Dynamic theme implementation;
  - Code enhancement and fixes;

- Snizhana (Team Fabulous Ostriches):
  - Login page creation;
  - Login form integration with full functionality and stylization;

- Yaroslav (Team Fabulous Ostriches):
  - User profile posts implementation;
  - Shortened user information display implementation;

- Dima (Team Fabulous Ostriches):
  - Message modal creation;
  - Flexible content modal implementation;

- Yura (Team Superhero Elephants - team leader):
  - Team management and coordination;
  - Home page feed development;
  - Netlify deployment;

- Maksim (Team Superhero Elephants):
  - UserProfile component creation;
  - Follow/unfollow user functionality;

- Nastya (Team Superhero Elephants):
  - Registration page addition;
  - Registration form development with full functionality and stylization;

## 📸 Screenshots

<details><summary>Login form</summary>
![Login form](https://i.imgur.com/oSy8A65.png)
</details>
<details><summary>Home page feed</summary>
![Home page feed](https://i.imgur.com/0Hio87e.png)
</details>
<details><summary>User page</summary>
![User page](https://i.imgur.com/R4voUHv.png)
</details>
<details><summary>Post modal window</summary>
![Post modal window](https://i.imgur.com/v0RL8dq.png)
</details>
<details><summary>Message modal with light theme</summary>
![Message modal with light theme](https://i.imgur.com/VrfYJJZ.png)
</details>
